<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>MyPage</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        <style>
            header{
                background-color:#273b66;
                text-align: center;
                font-size:30px;
                color:white;
                height: 65px;
                padding-top:10px;
            }
            
            nav{
                background-color: #23355b;
                height:65px;
                padding-left:40%;
            }
            
            nav div ul li{
                background-color:#273b66;
                color:white;
                width:90px;
                height:100%;
                text-align:center;
                padding-top:20px;
                padding-bottom:20px;
                border-left-style:solid;
                border-left-color:black;
                border-left-width:2px;
                border-right-style:solid;
                border-right-color:black;
                border-right-width:2px;
                float:left;
            }
            
            nav div ul li:hover{
                background-color:#526284;
            }
            
            nav div ul li:active{
                background-color:#0b111e;
            }
        </style>
</head>
<body>

<header>
            Cine LoMas
        </header>
        
        <div style="height:2px; background-color:black;" ></div>
        
        <nav>
        	<div>
	        	<ul style="overflow:hidden; list-style-type:none;">
		        	<%for(int i=0; i<3; i++) { %>
		            <li><%= i %></li>
		            <%} %>
	            </ul>
            </div>
            <!-- crear botones con un while-->
        </nav>
    
    
        <div style="width:20%; border-style:solid; border-width:5px;border-color:black; padding:10px; margin-left:40%; margin-top:150px;background-color:#273b66">
            <div class="field" style="">
                <form action="SendUser" method="post">
                    <p style="font-size: 20px; color:White">Username:</p>
                    <input type="text" name="username" style="width:100%"><br><br>
                    <p style="font-size: 20px; color:White">Password:</p>
                    <input type="password" name="password" style="width:100%">
                    <br><br>
                    <input type="submit" value="Log In" style="margin-left:39%">
                </form>
            </div>
        </div>
        <p style="text-align:center; font-size:13px">if you don't have an accont  <a href="/link"> sing in</a></p>
</body>
</html>