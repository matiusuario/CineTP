package entidades;

import java.util.ArrayList;

public class Cine {
	//variables
	private String nombre;
	private String direccion;
	private ArrayList<Sala> salas = new ArrayList<Sala>();
	private ArrayList<Socio> socios = new ArrayList<Socio>();
	
	//Getters y setters
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

} 
