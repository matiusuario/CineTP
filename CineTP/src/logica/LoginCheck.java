package logica;

import datos.DatosSocio;
import entidades.Socio;

public class LoginCheck {

	DatosSocio dp;
	
	public LoginCheck() {
		dp = new DatosSocio();
	}
	
	public static String makePassword( String username, String password) {
		return String.format("%d%d", Math.abs(username.subSequence(0, 5).hashCode()), Math.abs(password.hashCode()));
	}
	
	private boolean checkPass(String username, String textpass, Socio soc) {
		String savedPass = makePassword(username, textpass);
		
		if(soc.getPassword().equals(savedPass)) {
			return true;
		}else {
			return false;
		}
	}
	
	
	public Socio checkUp(String username, String password) throws Exception {
		
		Socio s = dp.getByUsername(username);
		
		if(checkPass(username,password, s)) {
			return s;
		}else {
			return null;
		}
		
				
	}
}
