package entidades;

public class Pelicula {
	//variables
	private String nombrePelicula;
	private int duracion;
	private int edad;		//Cambio edad a int
	private int precio;		//CAMBIO A INT POR PROBLEMAS CON SCANNER
	private String nombreImagen;
	
	//getters y setters
	public String getNombrePelicula() {
		return nombrePelicula;
	}
	public void setNombrePelicula(String nombre) {
		this.nombrePelicula = nombre;
	}
	
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public String getNombreImagen() {
		return nombreImagen;
	}
	public void setNombreImagen(String nombreImagen) {
		this.nombreImagen = nombreImagen;
	}
}