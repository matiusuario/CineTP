package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import entidades.*;
import logica.*;
//import datos.*;

@WebServlet({ "/SendUser", "/sendUser", "/Senduser", "/senduser"})


public class SendUser extends HttpServlet{
 
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		LoginCheck check = new LoginCheck();
		Socio soc = null;
		

		String uname =request.getAttribute("username").toString();
		String pass = request.getAttribute("password").toString();
		
		try {
			soc = check.checkUp(uname, pass);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(soc != null) {
			request.getSession().setAttribute("usuario", soc);
			
			//esto deberia apuntar al home
			request.getRequestDispatcher("index.html").forward(request, response);
		}
		
		//deberia mandar un mensaje dicioendo que no se pudo conectar
		request.getRequestDispatcher("LogIn2.jsp").forward(request, response);
		
	}
	
}
