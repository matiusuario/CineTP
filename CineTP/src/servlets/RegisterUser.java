package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import datos.DatosSocio;
import entidades.*;
import logica.*;

@WebServlet({ "/RegisterUser"})

public class RegisterUser extends HttpServlet{

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Socio soc = new Socio();
		boolean checkpass = true;
		String messange = "";
		
		if(request.getParameter("username").length() > 5) {		
			soc.setUsername(request.getParameter("username"));
		}else {
			checkpass = false;
			messange = "username demasiado corto.\n necesita tener minimo 6 letras\n";
		}
		
		if(request.getParameter("password").equals(request.getParameter("checkpassword")) && request.getParameter("password").length() > 5 && checkpass) {
			soc.setPassword(LoginCheck.makePassword(request.getParameter("username"), request.getParameter("password")));
		}else {
			if(checkpass) {
			checkpass = false;
			System.out.print("password demasiada corta.\n necesita tener minimo 6 letras\n");
			}
		}
		
		if(request.getParameter("email").equals(request.getParameter("checkemail")) && checkpass) {
			soc.setEmail(request.getParameter("email"));
		}else {
			if(checkpass) {
			checkpass = false;
			System.out.print("error email\n");
			}
		}
		
		
		if(checkpass) {
			soc.setEsAdmin(false);
			soc.setNombre(request.getParameter("nombre"));
			soc.setApellido(request.getParameter("apellido"));
			soc.setTelefono(Integer.parseInt(request.getParameter("telefono")));
			soc.setDni(Integer.parseInt(request.getParameter("dni")));
			
			try {
				DatosSocio.add(soc);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//mandar mensaje diciendo que se pudo cargar
			request.getRequestDispatcher("SingIn.jsp").forward(request, response);
			
		}else {
			
			request.setAttribute("messange", messange);
			request.getRequestDispatcher("SingIn2.jsp").forward(request, response);
		}
	}
}
