package logica;

import entidades.Pelicula;
import datos.DatosPelicula;
import java.util.ArrayList;

public class ControlABMPelicula {
	
	private DatosPelicula datosPeli;
	
	
	public ControlABMPelicula() {
		datosPeli = new DatosPelicula();
	}
	
	public void add(Pelicula p) throws Exception{
		datosPeli.add(p);
	}
	
	public void delete(Pelicula p)throws Exception{
		datosPeli.remove(p);
	}
	
/*	public void update(Pelicula p)throws Exception{
		datosPeli.update(p);
	}
 */	
	public ArrayList<Pelicula> getAll()throws Exception{
		return datosPeli.getAll();
	}
	
}
