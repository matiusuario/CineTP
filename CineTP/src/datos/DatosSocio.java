package datos;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import entidades.Socio;

public class DatosSocio {

	
	public ArrayList<Socio> getAll() throws Exception{
		Statement stmt = null;
		ResultSet rs = null;
		
		ArrayList<Socio> socios = new ArrayList<Socio>();
		try {
			stmt = SQLConexion.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from Socio");
			if(rs != null) {
				while(rs.next()) {
					Socio s = new Socio();
					s.setNombre(rs.getString("nombreSocio"));
					s.setApellido(rs.getString("apellidoSocio"));
					s.setDni(rs.getInt("dniSocio"));
					s.setEmail(rs.getString("mailSocio"));
					s.setTelefono(rs.getLong("telefonoSocio"));
					s.setNumeroSocio(rs.getInt("idSocio"));
					socios.add(s);
				}
			}
		}catch(Exception e) {
			throw e;
		}
		
		try {
			if(stmt != null) { stmt.close();}
			if(rs != null)rs.close();
			SQLConexion.getInstancia().releaseConn();
		}catch(SQLException e){
			throw e;
			
		}
		
		return socios;
	}
	
	public Socio getByUsername(String uname) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Socio soc = null;
		
		try {
			pstmt = SQLConexion.getInstancia().getConn().prepareStatement("select * from socio where username=?", PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, uname);
			pstmt.executeQuery();
			rs = pstmt.getGeneratedKeys();
			
			if(rs != null) {
				soc = new Socio();
				soc.setNombre(rs.getString("nombreSocio"));
				soc.setApellido(rs.getString("apellidoSocio"));
				soc.setDni(rs.getInt("dniSocio"));
				soc.setEmail(rs.getString("mailSocio"));
				soc.setTelefono(rs.getLong("telefonoSocio"));
				soc.setNumeroSocio(rs.getInt("idSocio"));
			}
		}catch(Exception e) {
			throw e;
		}finally {
			try {
				if(pstmt != null) {pstmt.close();}
				if(rs!= null) {rs.close();}
				SQLConexion.getInstancia().releaseConn();
			}catch(SQLException e) {
				throw e;
			}
		}
		
		return soc;
	}
	
	
	
	public static void add(Socio soc) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			pstmt = SQLConexion.getInstancia().getConn().prepareStatement("insert into Socio(`nombreSocio`, `apellidoSocio`, `dniSocio`, `telefonoSocio`, `mailSocio`, `esAdmin`, `username`, `password`) values(?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS); //faltan valores
			pstmt.setString(1, soc.getNombre());
			pstmt.setString(2, soc.getApellido());
			pstmt.setInt(3, soc.getDni());
			pstmt.setLong(4, soc.getTelefono());
			pstmt.setString(5, soc.getEmail());
			pstmt.setBoolean(6, soc.isEsAdmin());
			pstmt.setString(7, soc.getUsername());
			pstmt.setString(8, soc.getPassword());
			
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			
			//if(rs!= null && rs.next()) { System.out.print("se agrego");}
			
			
		}catch(Exception e) {
			throw e;
		}finally {
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			SQLConexion.getInstancia().releaseConn();
		}
	}
	public void remove(Socio soc) throws Exception {
		Statement stmt = null;
		
		try {
			stmt = SQLConexion.getInstancia().getConn().createStatement();
			stmt.executeUpdate("DELETE from Socio where idSocio = " + soc.getNumeroSocio());
		} catch (Exception e) { 
			throw e;
		} finally {
			if(stmt != null) stmt.close();
			SQLConexion.getInstancia().releaseConn();
		}
	}
}
