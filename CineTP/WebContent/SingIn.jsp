<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>MyPage</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        <style>
            header{
                background-color:#273b66;
                text-align: center;
                font-size:30px;
                color:white;
                height: 65px;
                padding-top:10px;
            }
            
            nav{
                background-color: #23355b;
                height:65px;
                padding-left:40%;
            }
            
            nav div{
                display:inline-block;
                background-color:#273b66;
                color:white;
                width:90px;
                height:100%;
                text-align:center;
                padding-top:20px;
                border-left-style:solid;
                border-left-color:black;
                border-left-width:2px;
                border-right-style:solid;
                border-right-color:black;
                border-right-width:2px;
            }
            
            nav div:hover{
                background-color:#526284;
            }
            
            nav div:active{
                background-color:#0b111e;
            }
            
            form p{
                font-size:16px;
                color:white;
            }
            
            form input{
                width:100%;
            }
        </style>
    </head>
    <body>
        <header>
            Cine LoMas
        </header>
        
        <div style="height:2px; background-color:black;" ></div>
        
        <nav>
            <div>home</div>
            <div>home</div>
            <!-- crear botones con un while-->
        </nav>
        
        <h2 style="text-align:center; margin-top:20px; margin-bottom:-100px">Sing In</h2>
        
        <%
        String[] baseValues = new String[7];
        for(int i = 0; i<7;i++){
        	baseValues[i] = "";
        }
        //baseValues[0] = "";
        System.out.print(baseValues[0]);
        if(request.getAttribute("messange") != null) {			
                    baseValues[0] = request.getParameter("username");
                    baseValues[1] = request.getParameter("email");
                    baseValues[2] = request.getParameter("checkemail");
                    baseValues[3] = request.getParameter("nombre");
                    baseValues[4] = request.getParameter("apellido");
                    baseValues[5] = request.getParameter("telefono");
                    baseValues[6] = request.getParameter("dni");
                    } %>
    
    
        <div style="width:20%; border-style:solid; border-width:5px;border-color:black; padding:10px; margin-left:40%; margin-top:115px;background-color:#273b66">
            <div class="field" style="">
                <form action="RegisterUser" method="post">
                    <p>Username:*</p>
                    <input type="text" value="<%= baseValues[0] %>" name="username"><br>
                    <p>Password:*</p>
                    <input type="password" name="password">
                    <br>
                    <p>Repeate password:*</p>
                    <input type="password" name="checkpassword">
                    <br>
                    <p>Email:*</p>
                    <input type="text" value="<%= baseValues[1] %>" name="email" >
                    <br>
                    <p>Repeate email:*</p>
                    <input type="text" value="<%= baseValues[2] %>" name="checkemail" >
                    <br><br>
                    
                    <p>_____________________________________</p>
                    <p style="font-size:12px">Datos Personales:</p>
                    
                    <br>
                    <p>Nombre:</p>
                    <input type="text" value="<%= baseValues[3] %>" name="nombre" >
                    <br>
                    <p>Apellido:</p>
                    <input type="text" value="<%= baseValues[4] %>" name="apellido" >
                    <br>
                    <p>telefono:</p>
                    <input type="text" value="<%= baseValues[5] %>" name="telefono" >
                    <br>
                    <p>dni:</p>
                    <input type="text" value="<%= baseValues[6] %>" name="dni" >
                    <br>
                    <br>
                    <%if(request.getAttribute("messange") != null) {%>
                    <p style="color:red"><%= request.getAttribute("messange")%></p>
                    <br>
                    <br>
                    <%} %>
                    <input type="submit" value="Sing In" style="margin-left:20%; width:60%">
                </form>
            </div>
        </div>
    </body>
</html>
