<%@ page import="entidades.Socio"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Cine LoMas</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        <style>
            #Title{
            	display:inline-block;
                text-align: left;
                font-size:30px;
                color:white;
                padding-top:10px;
                padding-left: 1%;
                width:50%;
            }
            
            #TitleBar{
            	height: 65px;
				background-color:#273b66;
            }
            
            #loginButon{
            	text-align:center;
            	height:45px;
            	width:100px;
            	background-color:#3c4e75;
            	position:absolute;
            	right:1%;
            	top:1.5%;
            	color:white;
            	line-height:2.5;
            	border-color:#b1b8c7;
            	border-width:2px;
            	border-style:solid;
            }
            
            #loginButon:hover{
            	background-color:#4f5f82;
            }
            
            #loginButon:active{
            	background-color:#0c0f17;
            	border-color:#2a3651;
            }
            
            nav{
                background-color: #23355b;
                height:65px;
                padding-left:1%;
            }
            
            nav a{
                display:inline-block;
                background-color:#273b66;
                color:white;
                width:90px;
                height:100%;
                text-align:center;
                border-left-style:solid;
                border-left-color:black;
                border-left-width:2px;
                border-right-style:solid;
                border-right-color:black;
                border-right-width:2px;
            }
            
            nav a:hover{
            	background-color:#4f5f82;
            	color:white;
            }
            
            nav a:active{
            	background-color:#0c0f17;
            	border-color:#2a3651;
            	color:white;
            }
        </style>
</head>
<body>

		<div id="TitleBar">
        	<p id="Title">
        	    Cine LoMas
       	 	</p>
        <%if(request.getSession().getAttribute("usuario") == null){ %>
        	<a id="loginButon" href="SingIn2.jsp"> Registrarse </a>
        	<%}else{ %>
        	<p style="position:absolute; right:1%; color:white; font-size:12"><%= "bienvenido " + ((Socio) request.getSession().getAttribute("usuario")).getNombre() + " " + ((Socio) request.getSession().getAttribute("usuario")).getApellido()  %></p>
        	<%} %>
        </div>
        <div style="height:2px; background-color:black;" ></div>
        
        <nav>
            <a href="index.html"><br>home</a>
            <a>comprar entrada</a>
            <!-- crear botones con un while-->
        </nav>
    
    
        <div style="width:20%; border-style:solid; border-width:5px;border-color:black; padding:10px; margin-left:40%; margin-top:150px;background-color:#273b66">
            <div class="field" style="">
                <form action="SendUser" method="post">
                    <p style="font-size: 20px; color:White">Username:</p>
                    <input type="text" name="username" style="width:100%"><br><br>
                    <p style="font-size: 20px; color:White">Password:</p>
                    <input type="password" name="password" style="width:100%">
                    <br><br>
                    <input type="submit" value="Log In" style="margin-left:39%">
                </form>
            </div>
        </div>
        <p style="text-align:center; font-size:13px">Si no ten�s una cuenta: cre� una. <a href="SingIn2.jsp"> Registrarse</a></p>
</body>
</html>