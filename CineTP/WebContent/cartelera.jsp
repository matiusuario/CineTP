<%@ page import="entidades.Pelicula"%>
<%@ page import="entidades.Socio"%>
<%@ page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="UTF-8">
		<title>CINE LoMas</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
<!--	  	<script defer src="https://use.fontawesome.com/releases/v5.12.0/js/all.js"></script>	 -->
<!--		<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">  -->
		<link rel="stylesheet" href="https://bulmatemplates.github.io/bulma-templates/css/cards.css">
		<style>
			
			#Title{
            	display:inline-block;
                text-align: left;
                font-size:30px;
                color:white;
                padding-top:10px;
                padding-left: 1%;
                width:50%;
            }
            
            #TitleBar{
            	height: 65px;
				background-color:#273b66;
            }
            
            #loginButon{
            	text-align:center;
            	height:45px;
            	width:100px;
            	background-color:#3c4e75;
            	position:absolute;
            	right:1%;
            	top:1.5%;
            	color:white;
            	line-height:2.5;
            	border-color:#b1b8c7;
            	border-width:2px;
            	border-style:solid;
            }
            
            #loginButon:hover{
            	background-color:#4f5f82;
            }
            
            #loginButon:active{
            	background-color:#0c0f17;
            	border-color:#2a3651;
            }
           	
            nav{
                background-color: #23355b;
                height:65px;
                
            }
            
            nav div{
                display:inline-block;
                background-color:#273b66;
                color:#ffffff;
                width:90px;
                height:100%;
                text-align:center;
               
            }
            
           
            
            nav div:active{
                background-color:#0b111e;
                color:#ffffff;
            }
            
            form p{
                font-size:16px;
                color:white;
            }
            
            form input{
                width:100%;
            }
			
        </style>
	</head>
<body>

		<div id="TitleBar">
        	<p id="Title">
        	    Cine LoMas
       	 	</p>
        <%if(request.getSession().getAttribute("usuario") != null){ %>
        	<p style="position:absolute; right:1%; color:white; font-size:12"><%= "Bienvenido " + ((Socio) request.getSession().getAttribute("usuario")).getNombre() + " " + ((Socio) request.getSession().getAttribute("usuario")).getApellido()  %></p>
        	<%} %>
        </div>
        
<!--    <nav>
            <a href="index.html"><br>home</a>
            <a>comprar entrada</a>
            <!-- crear botones con un while-->
<!--    </nav>   -->


        <!-- START NAV -->
        <nav class="navbar is-dark">
                  <div class="navbar-menu is-active">
                    	<div class="navbar-start" >
                            <a class="navbar-item" href="index.html">
                                    Inicio
                                </a>
                            <a class="navbar-item">
                            	Comprar Entradas
                            </a>
                        </div>
                        <div class="navbar-end">
                            <a class="navbar-item" href="LogIn2.jsp">
                                    Iniciar sesión
                                </a>
                         
                            <a class="navbar-item" href="SingIn2.jsp">
                                    Registrarse
                                </a>
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link">
                                        Cuenta
                                    </a>
                                <div class="navbar-dropdown">
                                    <a class="navbar-item">
                                            Perfil
                                        </a>
                                    <a class="navbar-item">
                                            Administración
                                        </a>
                                    <a class="navbar-item" href="/LogOut.jsp">
        								Cerrar sesion
        							</a>
      								</div>
                                </div>
                            </div>
                        </div>
      <!--          </div>
                </div>   -->
            </nav>
        <!--     END NAV -->
	
    <div class="container">
      <div class="section">
        <form action="Cartelera" method="get">
            <div class="box">
                <div class="field has-addons">
                    <div class="control is-expanded">
                        <input class="input has-text-centered" type="search" name="busqueda" placeholder="Buscar película">
                    </div>
                    <div class="control">
                 		<input type="submit" value="Buscar" class="button is-info">
                    </div>
                </div>
            </div>
    	 </form>
    	 
    	 <div class="row columns is-multiline">
        <% 
            	Pelicula p = new Pelicula();
            	ArrayList<Pelicula> pelis = new ArrayList<Pelicula>();
            	pelis = (ArrayList<Pelicula>)request.getAttribute("pelis");
 			//if (pelis.size() == 0 || pelis == null) {							%>
 	<!-- 		<div class="notification is-warning">
  					<button class="delete"></button>
						No se encontraron películas
				</div>		 -->
 		<%	//} else {
 				for(Pelicula peli : pelis){ 
            		String imName;
            		imName = "http://localhost:8080/CineTP/img/" + peli.getNombreImagen(); %>
            		<div class="column is-one-third">
                      <div class="card large round">
                        <div class="card-image ">
                            <figure class="image">
                          		
                                <img src=<%=imName%> alt="Image">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="media">
                                <div class="media-content">
                                    <p class="title is-4 no-padding"> <%= peli.getNombrePelicula().toString() %> </p> <br>
                                    <p class="subtitle is-6"> Clasif:<%if(peli.getEdad() == 0)%> ATP
                                    								<%if(peli.getEdad() > 0)%> +<%=peli.getEdad()%> </p>
                                </div>
                            </div>
                            <div class="content">
                                Duración: <%=peli.getDuracion() %> min. <br>
                            </div>
                        </div>
                      </div>
                	</div>
		<%		}
 			//}		%>
 			
            	
                
        </div>
      </div>
    </div>

    <footer class="footer">
            <div class="container">
            	<div class="content has-text-centered">
                    <p>
                        <strong>LoMas</strong> <a href="https://github.com/matiusuario/CineTP"> aplicación y sitio web</a> por Lucas y Matías.
                        The source code is licensed under <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPL</a>. <br>
                    </p>
                </div>
            </div>
    </footer>
	
</body>
</html>