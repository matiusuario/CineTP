package servlets;

import datos.*;
import entidades.*;
import logica.*;
import servlets.*;
import java.sql.*;
import java.util.Scanner;
import java.util.ArrayList;

public class EsUnaPrueba {

	public static void main(String[] args) throws Exception {
		
		try{
			Statement stmt = SQLConexion.getInstancia().getConn().createStatement();
			ControlABMPelicula ctrlP = new ControlABMPelicula();
			Scanner scan = new Scanner(System.in);
			int op = -1;
			System.out.println("MENU DE OPCIONES ");
			System.out.println("1... CARGAR PELICULA");
			System.out.println("2... VER CARTELERA");
			System.out.println("0... SALIR");
			
			while(op != 0) {
				op = scan.nextInt();
				switch (op) {
				case 1: {
					
					int dur, edad;
					int prec;
					String nom, cont;
					do {
						Pelicula peli = new Pelicula();
						scan.nextLine();
						System.out.println("Ingrese datos pelicula a agregar: ");
						System.out.println("Nombre: ");
						nom = scan.nextLine();
						System.out.println("precio: ");
						prec = scan.nextInt();
						System.out.println("duracion (min)");
						dur = scan.nextInt();
						System.out.println("edad minima: ");
						edad = scan.nextInt();
						
						System.out.println("Los datos:");
						System.out.println("  "+nom+ "  "+dur+ "min   +"+edad+ "  $"+prec+ "  ");
						
						peli.setNombrePelicula(nom);
						peli.setDuracion(dur);
						peli.setEdad(edad);
						peli.setPrecio(prec);
						ctrlP.add(peli);
						System.out.println("Carga exitosa ");
						System.out.println("¿Continuar cargando? (S/N)");
						cont = scan.nextLine();
						
					} while (cont.equalsIgnoreCase("S"));
					
				}
				scan.close();
				break;
				case (2): {
					ArrayList<Pelicula> pelis = ctrlP.getAll();
					for(Pelicula peli : pelis) {
						System.out.println(peli.getNombrePelicula() + "  " + "+"+peli.getEdad());
						System.out.println("$" + peli.getPrecio());
						System.out.println();
					}
				}
				break;
				case (0):;
				}
			}
			
			
		
		}catch (Exception e) { throw e;}
	}

}
