package logica;

import entidades.Sala;
import datos.DatosSala;
import java.util.ArrayList;

public class ControlABMSala {
	
	private DatosSala datosSal;
	
	
	public ControlABMSala() {
		datosSal = new DatosSala();
	}
	
	public void add(Sala s) throws Exception{
		datosSal.add(s);
	}
	
	public void delete(Sala s)throws Exception{
		datosSal.remove(s);
	}
	
	/*	public void update(Sala s)throws Exception{
			datosSal.update(s);
		}
	 */	
	public ArrayList<Sala> getAll()throws Exception{
		return datosSal.getAll();
	}
	
}
