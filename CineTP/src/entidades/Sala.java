package entidades;

import java.util.ArrayList;

public class Sala {
	//variables
	private int numeroSala;
	private int filas;
	private int columnas;
	private ArrayList<Funcion> funciones;
	
	public Sala() {
		funciones = new ArrayList<Funcion>();
	}
	
	//getters y Setters
	public int getNumeroSala() {
		return numeroSala;
	}
	public void setNumeroSala(int numero) {
		this.numeroSala = numero;
	}
	
	public int getFilas() {
		return filas;
	}
	public void setFilas(int filas) {
		this.filas = filas;
	}
	
	public int getColumnas() {
		return columnas;
	}
	public void setColumnas(int columnas) {
		this.columnas = columnas; 
	}
	
	
}
