package datos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalTime;

import entidades.Sala;
import entidades.Pelicula;
import entidades.Funcion;


public class DatosFuncion {
	
	public ArrayList<Funcion> getAll() throws Exception{
			
			Statement stmt = null;
			ResultSet rs = null;
			
			ArrayList<Funcion> funciones = new ArrayList<Funcion>(); 
			
			try {
				stmt = SQLConexion.getInstancia().getConn().createStatement();
				rs = stmt.executeQuery("select * from Funcion");
				if(rs!= null) {
					while(rs.next()) {
						Sala s;
						Pelicula p;
						DatosSala ds = new DatosSala();
						DatosPelicula dp = new DatosPelicula();
						s = ds.getSalasByID(rs.getInt("numeroSala"));
						p = dp.getPeliculaByNombre(rs.getString("nombrePelicula"));
						int yr = rs.getDate("fecha").getYear();
						int mn = rs.getDate("fecha").getMonth();
						int dy = rs.getDate("fecha").getDay();
						int hr = rs.getTime("horarioFuncion").getHours();
						int min = rs.getTime("horarioFuncion").getMinutes();
						Funcion f = new Funcion(s, p, yr, mn, dy, hr, min);
						funciones.add(f);
					}
				}
			}catch (Exception e) {
				throw e;
			}
			try {
				if(rs != null) {rs.close();}
				if(stmt!= null) {stmt.close();}
				SQLConexion.getInstancia().releaseConn();
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return funciones;
		}
		
		public Funcion getFuncionByID(int id) throws Exception  {
			Funcion f = null;
			PreparedStatement stmt = null;
			ResultSet rs = null;
			
			try {
				
				stmt = SQLConexion.getInstancia().getConn().prepareStatement("select * from Funcion where idFuncion=?");
				stmt.setInt(1, id);
				rs = stmt.executeQuery();
				
				if(rs!= null) {
					Sala s;
					Pelicula p;
					DatosSala ds = new DatosSala();
					DatosPelicula dp = new DatosPelicula();
					s = ds.getSalasByID(rs.getInt("numeroSala"));
					p = dp.getPeliculaByNombre(rs.getString("nombrePelicula"));
					int yr = rs.getDate("fecha").getYear();
					int mn = rs.getDate("fecha").getMonth();
					int dy = rs.getDate("fecha").getDay();
					int hr = rs.getTime("horarioFuncion").getHours();
					int min = rs.getTime("horarioFuncion").getMinutes();
					f = new Funcion(s, p, yr, mn, dy, hr, min);	
				}
				
			}catch(Exception e) {
				throw e;
			}
			
			try {
				if(rs != null) {rs.close();}
				if(stmt!= null) {stmt.close();}
				SQLConexion.getInstancia().releaseConn();
			}catch (SQLException e) {
				e.printStackTrace();
			}
			
			return f;
		}
		public void add(Funcion fun) throws Exception {
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {
				pstmt = SQLConexion.getInstancia().getConn().prepareStatement("insert into Funcion(idFuncion, nombrePelicula, numeroSala, fecha, horarioFuncion, horarioFin)" + " values(?,?,?,?,?,?)");
				pstmt.setInt(1, fun.getFuncionID());
				pstmt.setString(2, fun.getPelicula().getNombrePelicula());
				pstmt.setInt(3, fun.getSala().getNumeroSala());
				
				int yr, mo, dy, hr, min;
				yr = fun.getFecha().getYear();
				mo = fun.getFecha().getMonthValue();
				dy = fun.getFecha().getDayOfMonth();
				hr = fun.getHorario().getHour();
				min = fun.getHorario().getMinute();
				pstmt.setString(4, yr+"-"+mo+"-"+dy);
				pstmt.setString(5, hr+":"+min+":"+"00");
				LocalTime hrfin = fun.getHorario().plusMinutes(fun.getPelicula().getDuracion());
				int hrf = hrfin.getHour();
				int minf = hrfin.getMinute();
				pstmt.setString(6, hrf+":"+minf);
				pstmt.executeUpdate();
				rs = pstmt.getGeneratedKeys();
				
				//if(rs!= null && rs.next()) {}
				
			}catch(Exception e) {
				throw e;
			}finally {
				if(pstmt != null) pstmt.close();
				if(rs != null) rs.close();
				SQLConexion.getInstancia().releaseConn();
			}
		}
		public void remove(Funcion fun) throws Exception {
			Statement stmt = null;
			
			try {
				stmt = SQLConexion.getInstancia().getConn().createStatement();
				stmt.executeUpdate("DELETE from Funcion where idFuncion = " + fun.getFuncionID());
			} catch (Exception e) { 
				throw e;
			} finally {
				if(stmt != null) stmt.close();
				SQLConexion.getInstancia().releaseConn();
			}
		}
}