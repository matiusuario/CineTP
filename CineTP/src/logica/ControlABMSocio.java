package logica;

import entidades.Socio;
import datos.DatosSocio;
import java.util.ArrayList;

public class ControlABMSocio {
	
	private DatosSocio datosSoc;
	
	
	public ControlABMSocio() {
			datosSoc = new DatosSocio();
	}
		
	public void add(Socio s) throws Exception{
		datosSoc.add(s);
	}
	
	public void delete(Socio s)throws Exception{
		datosSoc.remove(s);
	}
	
	/*	public void update(Socio s)throws Exception{
			datosSoc.update(s);
		}
	 */	
	public ArrayList<Socio> getAll()throws Exception{
		return datosSoc.getAll();
	}
	
}
