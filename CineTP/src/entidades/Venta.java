package entidades;

public class Venta {
	//variables
	private Socio socio;
	private Funcion funcion; 
	private int cantidad;
	private double subtotal;
	private int[] butaca;
	
	//getters y setters
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal() {
		this.subtotal = funcion.getPelicula().getPrecio() * cantidad;
	}

}
