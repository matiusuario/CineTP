package datos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import entidades.Sala;

public class DatosSala {
	
	public ArrayList<Sala> getAll() throws Exception{
		
		Statement stmt = null;
		ResultSet rs = null;
		
		ArrayList<Sala> salas = new ArrayList<Sala>(); 
		
		try {
			stmt = SQLConexion.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from Sala");
			if(rs!= null) {
				while(rs.next()) {
					Sala s = new Sala();
					s.setNumeroSala(rs.getInt("idSala"));
					s.setColumnas(rs.getInt("columSala"));
					s.setFilas(rs.getInt("filasSala"));
					salas.add(s);
				}
			}
		}catch (Exception e) {
			throw e;
		}
		try {
			if(rs != null) {rs.close();}
			if(stmt!= null) {stmt.close();}
			SQLConexion.getInstancia().releaseConn();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return salas;
	}
	
	public Sala getSalasByID(int idSala) throws Exception  {
		Sala s = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			
			stmt = SQLConexion.getInstancia().getConn().prepareStatement("select * from Sala where idSala=?");
			stmt.setInt(1, idSala);
			rs = stmt.executeQuery();
			
			if(rs!= null) {
				s = new Sala();
				s.setNumeroSala(rs.getInt("idSala"));
				s.setColumnas(rs.getInt("columSala"));
				s.setFilas(rs.getInt("filaSala"));
			}
			
		}catch(Exception e) {
			throw e;
		}
		
		try {
			if(rs != null) {rs.close();}
			if(stmt!= null) {stmt.close();}
			SQLConexion.getInstancia().releaseConn();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		return s;
	}
	public void add(Sala sal) throws Exception {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			pstmt = SQLConexion.getInstancia().getConn().prepareStatement("insert into Sala(filasSala, columSala)" + " values(?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, sal.getFilas());
			pstmt.setInt(2, sal.getColumnas());
			pstmt.executeUpdate();
			rs = pstmt.getGeneratedKeys();
			
			//if(rs!= null && rs.next()) {}
			
		}catch(Exception e) {
			throw e;
		}finally {
			if(pstmt != null) pstmt.close();
			if(rs != null) rs.close();
			SQLConexion.getInstancia().releaseConn();
		}
	}
	public void remove(Sala sal) throws Exception {
		Statement stmt = null;
		
		try {
			stmt = SQLConexion.getInstancia().getConn().createStatement();
			stmt.executeUpdate("DELETE from Sala where idSala = " + sal.getNumeroSala());
		} catch (Exception e) { 
			throw e;
		} finally {
			if(stmt != null) stmt.close();
			SQLConexion.getInstancia().releaseConn();
		}
	}
}
