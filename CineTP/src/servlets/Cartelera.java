package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.io.PrintWriter;

import datos.DatosPelicula;
import entidades.Pelicula;

/**
 * Servlet implementation class Cartelera
 */
@WebServlet({ "/Cartelera", "/cartelera", "/index.html", /*"/cartelera.jsp"*/ })
public class Cartelera extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cartelera() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			DatosPelicula dp = new DatosPelicula();
			String b = request.getParameter("busqueda");
			ArrayList<Pelicula> pelis = new ArrayList<Pelicula>();
			if(b!= null) {
				pelis = dp.SearchIt(b);
			}else {
				pelis = dp.getAll();
			}
			if(pelis.size() > 0) request.setAttribute("pelis", pelis);
			else {
				PrintWriter out = response.getWriter();
				out.println("No se ha encontrado la película ");
			}
			request.getRequestDispatcher("cartelera.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
