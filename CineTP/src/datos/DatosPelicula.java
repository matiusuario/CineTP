package datos;

//import java.sql.Statement;
//import java.sql.ResultSet;
import java.sql.*;
import java.util.ArrayList;

import entidades.Pelicula;
import entidades.Sala;


public class DatosPelicula {

	public ArrayList<Pelicula> getAll() throws Exception{
			
			Statement stmt=null;
			ResultSet rs=null;
			ArrayList<Pelicula> pelis = new ArrayList<Pelicula>();
			try{
				stmt = SQLConexion.getInstancia().getConn().createStatement();
				rs = stmt.executeQuery("select * from Pelicula");		//CUIDADO
				if(rs != null) {
					while(rs.next()) {
						Pelicula p = new Pelicula();
						p.setNombrePelicula(rs.getString("nombrePelicula"));
						p.setDuracion(rs.getInt("duracion"));
						p.setEdad(rs.getInt("edadMinima"));
						p.setPrecio(rs.getInt("precio"));
						p.setNombreImagen(rs.getString("nombreImagen"));
						pelis.add(p);
					}
				}
			} catch (Exception e){
				throw e;
			}
			
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
				SQLConexion.getInstancia().releaseConn();
			} catch (SQLException e) {
				e.printStackTrace();
			}	
			return pelis;
	}
	
	public Pelicula getPeliculaByNombre(String nombrePeli) throws Exception  {
		Pelicula p = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			
			stmt = SQLConexion.getInstancia().getConn().prepareStatement("select * from Pelicula where nombrePelicula=?");
			stmt.setString(1, nombrePeli);
			rs = stmt.executeQuery();
			
			if(rs!= null) {
				p = new Pelicula();
				p.setNombrePelicula(rs.getString("nombrePelicula"));
				p.setDuracion(rs.getInt("duracion"));
				p.setEdad(rs.getInt("edadMinima"));
				p.setPrecio(rs.getInt("precio"));
				p.setNombreImagen(rs.getString("nombreImagen"));
			}
			
		}catch(Exception e) {
			throw e;
		}
		
		try {
			if(rs != null) {rs.close();}
			if(stmt!= null) {stmt.close();}
			SQLConexion.getInstancia().releaseConn();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}
	
	public ArrayList<Pelicula> SearchIt(String nombreBuscar) throws Exception{
		if(nombreBuscar == "") {
			return getAll();
		}
		
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<Pelicula> result = new ArrayList<Pelicula>();
		try {
			stmt = SQLConexion.getInstancia().getConn().createStatement();
			rs = stmt.executeQuery("select * from Pelicula");
			if(rs != null) {
				while(rs.next()) {
					if(rs.getString("nombrePelicula").toLowerCase().contains(nombreBuscar.toLowerCase())) {
						Pelicula p = new Pelicula();
						p.setNombrePelicula(rs.getString("nombrePelicula"));
						p.setDuracion(rs.getInt("duracion"));
						p.setEdad(rs.getInt("edadMinima"));
						p.setPrecio(rs.getInt("precio"));
						p.setNombreImagen(rs.getString("nombreImagen"));
						result.add(p);
					}
				}
			}
		}catch(Exception e){
			throw e;
		}
		
		return result;
	}
	
	public void add(Pelicula p) throws Exception {
		if(p == null) {
			return;
		}
		
		PreparedStatement stmt = null;
		try {
			stmt = SQLConexion.getInstancia().getConn().prepareStatement("insert into Pelicula (nombrePelicula, duracion, edadMinima, precio, nombreImagen) values (?,?,?,?,?)");
			stmt.setString(1, p.getNombrePelicula());
			stmt.setInt(2, p.getDuracion());
			stmt.setInt(3, p.getEdad());
			stmt.setDouble(4, p.getPrecio());
			stmt.setString(5, p.getNombreImagen());
			
			stmt.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
			
		}finally {
			try{
			if(stmt!=null) {stmt.close();}
			SQLConexion.getInstancia().releaseConn();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	//cambiarlo para que devuelva un mensaje al terminar
	public void remove(Pelicula p) throws SQLException{
		PreparedStatement stmt = null;
		try {
			stmt = SQLConexion.getInstancia().getConn().prepareStatement("delete from Pelicula where nombrePelicula=?");
			stmt.setString(1, p.getNombrePelicula());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		 finally {
				if(stmt != null) stmt.close();
				SQLConexion.getInstancia().releaseConn();
		 }
	}
}