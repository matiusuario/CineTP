<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <title>Cine LoMas</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        <style>
            #Title{
            	display:inline-block;
                text-align: left;
                font-size:30px;
                color:white;
                padding-top:10px;
                padding-left: 1%;
                width:50%;
            }
            
            #TitleBar{
            	height: 65px;
				background-color:#273b66;
            }
            
            #loginButon{
            	text-align:center;
            	height:45px;
            	width:100px;
            	background-color:#3c4e75;
            	position:absolute;
            	right:1%;
            	top:1.5%;
            	color:white;
            	line-height:2.5;
            	border-color:#b1b8c7;
            	border-width:2px;
            	border-style:solid;
            }
            
            #loginButon:hover{
            	background-color:#4f5f82;
            }
            
            #loginButon:active{
            	background-color:#0c0f17;
            	border-color:#2a3651;
            }
            
            nav{
                background-color: #23355b;
                height:65px;
                padding-left:1%;
            }
            
            nav div{
                display:inline-block;
                background-color:#273b66;
                color:white;
                width:90px;
                height:100%;
                text-align:center;
                border-left-style:solid;
                border-left-color:black;
                border-left-width:2px;
                border-right-style:solid;
                border-right-color:black;
                border-right-width:2px;
            }
            
            nav div:hover{
                background-color:#526284;
            }
            
            nav div:active{
                background-color:#0b111e;
            }
            
            form p{
                font-size:16px;
                color:white;
            }
            
            form input{
                width:100%;
            }
        </style>
    </head>
    <body>
    	<div id="TitleBar">
        	<p id="Title">
        	    Cine LoMas
       	 	</p>
        
        	<a id="loginButon" href="LogIn2.jsp"> Login </a>
        </div>
        <div style="height:2px; background-color:black;" ></div>
        
        <nav>
            <div><p>.</p><p>home</p></div>
            <div><p>comprar</p><p> entrada</p></div>
            <!-- crear botones con un while-->
        </nav>
        
        <h2 style="text-align:center; margin-top:70px; margin-bottom:-100px">Sing In</h2>
    
    	  <%
        String[] baseValues = new String[7];
        for(int i = 0; i<7;i++){
        	baseValues[i] = "";
        }
        //baseValues[0] = "";
        System.out.print(baseValues[0]);
        if(request.getAttribute("messange") != null) {			
                    baseValues[0] = request.getParameter("username");
                    baseValues[1] = request.getParameter("email");
                    baseValues[2] = request.getParameter("checkemail");
                    baseValues[3] = request.getParameter("nombre");
                    baseValues[4] = request.getParameter("apellido");
                    baseValues[5] = request.getParameter("telefono");
                    baseValues[6] = request.getParameter("dni");
        } %>
    
        <div style="width:40%; border-style:solid; border-width:5px;border-color:black; padding:10px; margin-left:30%; margin-top:200px;background-color:#273b66">
            <div class="field" style="">
                <form action="RegisterUser" method="post">
                	<div style="display:inline-block; width:45%; margin-right:10px;">
	                    <p>Username:*</p>
	                    <input type="text" value="<%= baseValues[0] %>" name="username"><br>
	                    <p>Password:*</p>
	                    <input type="password" name="password">
	                    <br>
	                    <p>Repeate password:*</p>
	                    <input type="password" name="checkpassword">
	                    <br>
	                    <p>Email:*</p>
	                    <input type="text" value="<%= baseValues[1] %>" name="email" >
	                    <br>
	                    <p>Repeate email:*</p>
	                    <input type="text" value="<%= baseValues[2] %>" name="checkemail" >
                    	<br><br>
                    </div>
                    <!--<p>____________________________________________</p>
                      <p style="font-size:12px">Datos Personales:</p>-->
                    <div style="display:inline-block; width:45%;">
                    	<p style="font-size:12px">Datos Personales (no son necesarios):</p> 
	                    <br>
	                    <p>Nombre:</p>
	                    <input type="text" value="<%= baseValues[3] %>" name="nombre" >
	                    <br>
	                    <p>Apellido:</p>
	                    <input type="text" value="<%= baseValues[4] %>" name="apellido" >
	                    <br>
	                    <p>telefono:</p>
	                    <input type="text" value="<%= baseValues[5] %>" name="telefono" >
	                    <br>
	                    <p>dni:</p>
	                    <input type="text" value="<%= baseValues[6] %>" name="dni" >
	                    <br><br>
                    </div>
                    <%if(request.getAttribute("messange") != null) {%>
                    <p style="color:red"><%= request.getAttribute("messange")%></p>
                    <br>
                    <%} %>
                    
                    <br>
                    <input type="submit" value="Sing In" style="margin-left:20%; width:60%">
                </form>
            </div>
        </div>
    </body>
</html>