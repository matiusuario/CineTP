package entidades;

import java.time.LocalTime;
import java.time.LocalDate;


public class Funcion {
	//variables
	private static int allFuncionID; 
	private int funcionID;
	private LocalDate fecha;
	private LocalTime horario;
	private LocalTime horarioFin;
	private boolean[][] butacasOcupadas;
	private Sala sala;
	private Pelicula pelicula;
	
	
	
	//getters y setters
	
	public LocalDate getFecha() {
		return fecha;
	}
	
	public LocalTime getHora() {
		return horario;
	}
	public void setFecha(int year, int mes, int dia, int hora, int minutos) {
		this.horario = LocalTime.of(hora, minutos);
		this.fecha = LocalDate.of (year, mes, dia);
	}
	public Sala getSala() {
		return sala;
	}
	public void setNumeroSala(Sala sala) {
		this.sala = sala;
	}
	public Pelicula getPelicula() {
		return pelicula;
	}
	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}
	
	public int getFuncionID() {
		return funcionID;
	}
	private void setFuncionID() {
		this.funcionID = allFuncionID;
	}
	 public Funcion(Sala sala, Pelicula pelicula, int year, int mes,int dia, int hora, int min) {
		 allFuncionID++;
		 this.setFuncionID();
		 
		 this.sala = sala;
		 butacasOcupadas = new boolean[sala.getFilas()][sala.getColumnas()];
		 this.pelicula = pelicula;
		 
		 this.setFecha(year, mes, dia, hora, min);
	 }

	public LocalTime getHorario() {
		return horario;
	}

	public void setHorario(LocalTime horario) {
		this.horario = horario;
	}

	public LocalTime getHorarioFin() {
		return horarioFin;
	}

	public void setHorarioFin(LocalTime horarioFin) {
		this.horarioFin = horarioFin;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}
		
	
}